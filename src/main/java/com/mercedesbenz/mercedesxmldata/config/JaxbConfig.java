package com.mercedesbenz.mercedesxmldata.config;

import com.mercedesbenz.mercedesxmldata.services.DataService;
import com.mercedesbenz.mercedesxmldata.services.xmlData.DataXml;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@Configuration
public class JaxbConfig {

    @Bean
    JAXBContext jaxbContext() throws  JAXBException {
        return JAXBContext.newInstance(DataXml.class);
    }

    @Bean
    public Unmarshaller unmarshaller(JAXBContext jaxbContext) throws JAXBException {
        return jaxbContext.createUnmarshaller();
    }

    @Bean
    public Marshaller marshaller(JAXBContext jaxbContext) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return marshaller;
    }

    @Bean
    public CommandLineRunner commandLineRunner(DataService dataService) {
        return args -> dataService.uploadData();
    }

}
