package com.mercedesbenz.mercedesxmldata.services;

import com.mercedesbenz.mercedesxmldata.controllers.dto.DataDTO;
import com.mercedesbenz.mercedesxmldata.controllers.dto.EpisodeDTO;

import java.util.List;

public interface EpisodeService {
    EpisodeDTO findEpisodeById(Long episodeId);
    List<EpisodeDTO> findAllEpisodes();
    boolean deleteEpisodeById(Long episodeId);

    DataDTO findEpisodeBySerie(Long serieId);
}
