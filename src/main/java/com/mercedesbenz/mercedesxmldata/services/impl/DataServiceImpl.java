package com.mercedesbenz.mercedesxmldata.services.impl;

import com.mercedesbenz.mercedesxmldata.model.entities.Episode;
import com.mercedesbenz.mercedesxmldata.model.entities.Genre;
import com.mercedesbenz.mercedesxmldata.model.entities.Person;
import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import com.mercedesbenz.mercedesxmldata.model.enums.PersonType;
import com.mercedesbenz.mercedesxmldata.model.repositories.EpisodeRepository;
import com.mercedesbenz.mercedesxmldata.model.repositories.GenreRepository;
import com.mercedesbenz.mercedesxmldata.model.repositories.PersonRepository;
import com.mercedesbenz.mercedesxmldata.model.repositories.SerieRepository;
import com.mercedesbenz.mercedesxmldata.services.DataService;
import com.mercedesbenz.mercedesxmldata.services.xmlData.DataXml;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service("dataServiceImpl")
public class DataServiceImpl implements DataService {
    private Unmarshaller unmarshaller;
    private Marshaller marshaller;
    private PersonRepository personRepository;
    private GenreRepository genreRepository;
    private SerieRepository serieRepository;
    private EpisodeRepository episodeRepository;

    @Override
    public DataXml findDataByFilename(String filename) throws IOException, JAXBException {
        Resource resource = getXMLResources(filename).orElseThrow(
                () -> new RuntimeException("file do not exist")
        );
        return (DataXml) unmarshaller.unmarshal(resource.getInputStream());
    }

    @Override
    public List<DataXml> findAll() throws IOException {
        Resource[] xml = getXMLResources();
        return Arrays.stream(xml)
                .map( resource -> {
                    try {
                        return (DataXml) unmarshaller.unmarshal(resource.getInputStream());
                    } catch (JAXBException  | IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void uploadData()  {

        try {
            Resource[]  xml = getXMLResources();
            Arrays.stream(xml)
                    .map( resource -> {
                        try {
                            return (DataXml) unmarshaller.unmarshal(resource.getInputStream());
                        } catch (JAXBException e) {
                            throw new RuntimeException(e);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .collect(Collectors.toList())
                    .forEach(dataXml -> saveData(dataXml));
        } catch (  IOException e) {
            throw new RuntimeException(e);
        }

    }

    private Resource[] getXMLResources() throws IOException {
        ClassLoader classLoader = MethodHandles.lookup().getClass().getClassLoader();
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        return resolver.getResources("classpath:xmldb/*.xml");
    }

    private Optional<Resource> getXMLResources(String filename) throws IOException {
        ClassLoader classLoader = MethodHandles.lookup().getClass().getClassLoader();
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        filename = "classpath:xmldb/" + filename + ".xml";
        return Arrays.stream(resolver.getResources(filename)).findFirst();
    }


    private void savePerson(List<Person> persons, PersonType personType) {

        personRepository.saveAll(
                persons.stream().map(person -> {
                            person.setPersonType(personType);
                            return person;
                        })
                        .collect(Collectors.toList())
        );
    }

    private void saveGenre(List<Genre> genes) {
        genreRepository.saveAll(genes);
    }

    private void saveSerie(Serie serie) {
        serieRepository.save(serie);
    }
    private void saveEpisode(List<Episode> episodes, Serie serie) {

        for (Episode episode : episodes) {
            savePerson(episode.getGuestStar(), PersonType.GUEST_STAR);
            savePerson(episode.getWriters(), PersonType.WRITER);
            savePerson(episode.getDirector(), PersonType.DIRECTOR);
            episode.setSerie(serie);
            episodeRepository.save(episode);

        }
    }

    private void saveData(DataXml dataXml) {
        Serie serie = dataXml.getSerie();
        savePerson(serie.getActors(), PersonType.ACTOR);
        saveGenre(serie.getGenres());
        saveSerie(serie);

        List<Episode> episodes = dataXml.getEpisodes();

        saveEpisode(episodes, serie);
    }
}
