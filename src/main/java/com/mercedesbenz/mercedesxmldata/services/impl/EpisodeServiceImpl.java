package com.mercedesbenz.mercedesxmldata.services.impl;

import com.mercedesbenz.mercedesxmldata.controllers.dto.DataDTO;
import com.mercedesbenz.mercedesxmldata.controllers.dto.EpisodeDTO;
import com.mercedesbenz.mercedesxmldata.controllers.dto.SerieDTO;
import com.mercedesbenz.mercedesxmldata.model.entities.Episode;
import com.mercedesbenz.mercedesxmldata.model.repositories.EpisodeRepository;
import com.mercedesbenz.mercedesxmldata.services.EpisodeService;
import com.mercedesbenz.mercedesxmldata.services.SerieService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("episodeServiceImpl")
@AllArgsConstructor
public class EpisodeServiceImpl implements EpisodeService {

    @Qualifier("serieServiceImpl")
    private SerieService serieService;
    private EpisodeRepository episodeRepository;
    @Override
    public EpisodeDTO findEpisodeById(Long episodeId) {
        return findEpisode(episodeId).toDto();
    }

    @Override
    public List<EpisodeDTO> findAllEpisodes() {
        return episodeRepository.findAll()
                .stream()
                .map( episode -> episode.toDto())
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteEpisodeById(Long episodeId) {

        episodeRepository.delete(findEpisode(episodeId));
        return false;
    }

    @Override
    public DataDTO findEpisodeBySerie(Long serieId) {
        SerieDTO serie = serieService.findSerieById(serieId);
        List<EpisodeDTO> episodes = episodeRepository.findEpisodeBySerie(serieId)
                .stream()
                .map( episode -> episode.toDto())
                .collect(Collectors.toList());

        return DataDTO.builder()
                .serie(serie)
                .episodes(episodes)
                .build();
    }

    private Episode findEpisode(Long episodeId) {
        return episodeRepository.findById(episodeId).orElseThrow(()-> new RuntimeException("Episode not fund"));
    }

}
