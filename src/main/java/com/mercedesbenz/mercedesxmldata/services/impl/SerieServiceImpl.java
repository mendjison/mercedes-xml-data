package com.mercedesbenz.mercedesxmldata.services.impl;

import com.mercedesbenz.mercedesxmldata.controllers.dto.SerieDTO;
import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import com.mercedesbenz.mercedesxmldata.model.repositories.SerieRepository;
import com.mercedesbenz.mercedesxmldata.services.SerieService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("serieServiceImpl")
@AllArgsConstructor
public class SerieServiceImpl implements SerieService {

    private SerieRepository serieRepository;

    @Override
    public SerieDTO findSerieById(Long serieId) {
        return findSerie(serieId).toDto();
    }

    @Override
    public List<SerieDTO> findAllSeries() {
        return serieRepository.findAll()
                .stream()
                .map(Serie::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteSerieById(Long serieId) {
        serieRepository.delete(findSerie(serieId));
        return true;
    }

    private Serie findSerie(Long serieId) {
        return serieRepository.findById(serieId).orElseThrow(()-> new RuntimeException("Serie not fund"));
    }
}
