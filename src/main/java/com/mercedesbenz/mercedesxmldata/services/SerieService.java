package com.mercedesbenz.mercedesxmldata.services;

import com.mercedesbenz.mercedesxmldata.controllers.dto.SerieDTO;

import java.util.List;

public interface SerieService {
    SerieDTO findSerieById(Long serieId);
    List<SerieDTO> findAllSeries();
    boolean deleteSerieById(Long serieId);
}
