package com.mercedesbenz.mercedesxmldata.services;

import com.mercedesbenz.mercedesxmldata.services.xmlData.DataXml;


import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

public interface DataService {
    DataXml findDataByFilename(String filename) throws IOException, JAXBException;
    List<DataXml> findAll() throws IOException, JAXBException;

    void uploadData();
}
