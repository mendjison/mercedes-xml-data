package com.mercedesbenz.mercedesxmldata.services.xmlData;


import com.mercedesbenz.mercedesxmldata.model.entities.Episode;
import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Data")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public class DataXml {
    @XmlElement(name = "Series")
    private Serie serie;
    @XmlElement(name = "Episode")
    private List<Episode> episodes;
}
