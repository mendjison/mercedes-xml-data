package com.mercedesbenz.mercedesxmldata.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DataDTO {

    private SerieDTO serie;
    private List<EpisodeDTO> episodes;
}
