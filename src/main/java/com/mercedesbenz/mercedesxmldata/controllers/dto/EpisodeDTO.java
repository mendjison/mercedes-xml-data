package com.mercedesbenz.mercedesxmldata.controllers.dto;

import com.mercedesbenz.mercedesxmldata.model.entities.Episode;
import com.mercedesbenz.mercedesxmldata.model.entities.Person;
import com.mercedesbenz.mercedesxmldata.model.enums.Language;
import com.mercedesbenz.mercedesxmldata.model.enums.PersonType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EpisodeDTO {

    private long id;
    private int combinedEpisodenumber;
    private int combinedSeason;
    private String dvdChapter;
    private String dvd_discid;
    private String dvdEpisodenumber;
    private String dvdSeason;
    private List<String> director;
    private int epImgFlag;
    private String episodeName;
    private int episodeNumber;
    private LocalDate firstAired;
    private List<String> guestStar = new ArrayList<>();
    private String imdbID;
    private Language language;
    private String overview;
    private String productionCode;
    private double rating;
    private int ratingCount;
    private int seasonNumber;
    private List<String> writers = new ArrayList<>();
    private int absoluteNumber;
    private int airsafterSeason;
    private int airsbeforeEpisode;
    private int airsbeforeSeason;
    private String filename;
    private int isMovie;
    private long lastupdated;
    private long seasonid;
    private long seriesid;
    private LocalDateTime thumbAdded;
    private long thumbHeight;
    private long thumbWidth;
    private SerieDTO serie;

    public Episode toPojo() {
        return Episode.builder()
                .id(this.getId())
                . combinedEpisodenumber(this.getCombinedEpisodenumber())
                .combinedSeason(this.getCombinedSeason())
                .dvdChapter(this.getDvdChapter())
                .dvd_discid(this.getDvd_discid())
                .dvdEpisodenumber(this.getDvdEpisodenumber())
                .dvdSeason(this.getDvdSeason())
                .director(this.getDirector().stream().map(director -> new Person(director, PersonType.DIRECTOR)).collect(Collectors.toList()))
                .epImgFlag(this.getEpImgFlag())
                .episodeName(this.getEpisodeName())
                .episodeNumber(this.getEpisodeNumber())
                .firstAired(this.getFirstAired())
                .guestStar(this.getGuestStar().stream().map(guestStar -> new Person(guestStar, PersonType.GUEST_STAR)).collect(Collectors.toList()))
                . imdbID(this.getImdbID())
                .language(this.getLanguage())
                .overview(this.getOverview())
                .productionCode(this.getProductionCode())
                .rating(this.getRating())
                .ratingCount(this.getRatingCount())
                .seasonNumber(this.getSeasonNumber())
                .writers(this.getWriters().stream().map(writer -> new Person(writer, PersonType.WRITER)).collect(Collectors.toList()))
                .absoluteNumber(this.getAbsoluteNumber())
                .airsafterSeason(this.getAirsafterSeason())
                .airsbeforeEpisode(this.getAirsbeforeEpisode())
                .airsbeforeSeason(this.getAirsbeforeSeason())
                .filename(this.getFilename())
                .isMovie(this.getIsMovie())
                .lastupdated(this.getLastupdated())
                .seasonid(this.getSeasonid())
                .seriesid(this.getSeriesid())
                .thumbAdded(this.getThumbAdded())
                .thumbHeight(this.getThumbHeight())
                .thumbWidth(this.getThumbWidth())
                .serie(this.getSerie().toPojo())
                .build();
    }
}
