package com.mercedesbenz.mercedesxmldata.controllers.dto;

import com.mercedesbenz.mercedesxmldata.model.entities.Genre;
import com.mercedesbenz.mercedesxmldata.model.entities.Person;
import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import com.mercedesbenz.mercedesxmldata.model.enums.AirsDayOfWeek;
import com.mercedesbenz.mercedesxmldata.model.enums.Language;
import com.mercedesbenz.mercedesxmldata.model.enums.PersonType;
import com.mercedesbenz.mercedesxmldata.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SerieDTO {

    private long id;
    private List<String> actors = new ArrayList<>();
    private AirsDayOfWeek airsDayOfWeek;
    private LocalTime airsTime;
    private String contentRating;
    private LocalDate firstAired;
    private List<String> genres = new ArrayList<>();
    private String imdbId;
    private Language language;
    private String network;
    private String networkID;
    private String overview;
    private double rating;
    private int ratingCount;
    private int runtime;
    private long seriesID;
    private String seriesName;
    private Status status;
    private LocalDateTime added;
    private long addedBy;
    private String banner;
    private String fanart;
    private LocalDate  finaleAired;
    private long lastupdated;
    private String poster;
    private int tmsWanteOld;
    private int zap2itID;

    public Serie toPojo() {
        return Serie.builder()
                .id(this.getSeriesID())
                .actors(this.getActors().stream().map(actor -> new Person(actor, PersonType.ACTOR)).collect(Collectors.toList()))
                .airsDayOfWeek(this.getAirsDayOfWeek())
                .airsTime(this.getAirsTime())
                .contentRating(this.getContentRating())
                .firstAired(this.getFirstAired())
                .genres(this.getGenres().stream().map(genre -> new Genre(genre)).collect(Collectors.toList()))
                .imdbId(this.getImdbId())
                .language(this.getLanguage())
                .network(this.getNetwork())
                .overview(this.getOverview())
                .rating(this.getRating())
                .ratingCount(this.getRatingCount())
                .runtime(this.getRuntime())
                .seriesID(this.getSeriesID())
                .seriesName(this.getSeriesName())
                .status(this.getStatus())
                .added(this.getAdded())
                .addedBy(this.getAddedBy())
                .banner(this.getBanner())
                .fanart(this.getFanart())
                .finaleAired(this.getFinaleAired())
                .lastupdated(this.getLastupdated())
                .poster(this.getPoster())
                .tmsWanteOld(this.getTmsWanteOld())
                .zap2itID(this.getZap2itID())
                .build();
    }
}
