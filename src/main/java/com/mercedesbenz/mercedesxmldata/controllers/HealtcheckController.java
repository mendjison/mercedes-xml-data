package com.mercedesbenz.mercedesxmldata.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HealtcheckController {

    @GetMapping("/check")
    public ResponseEntity<Void> healthcheckliveness() {
        return ResponseEntity.ok().build();
    }
}
