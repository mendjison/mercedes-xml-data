package com.mercedesbenz.mercedesxmldata.controllers;

import com.mercedesbenz.mercedesxmldata.controllers.dto.DataDTO;
import com.mercedesbenz.mercedesxmldata.controllers.dto.EpisodeDTO;
import com.mercedesbenz.mercedesxmldata.services.EpisodeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/episodes")
@AllArgsConstructor
public class EpisodeController {

    @Qualifier("episodeServiceImpl")
    private EpisodeService episodeService;

    @GetMapping("/{episodeId}")
    public ResponseEntity<EpisodeDTO> getEpisode(@PathVariable("episodeId") Long episodeById) {
        return ResponseEntity.ok()
                .body(episodeService.findEpisodeById(episodeById));
    }

    @GetMapping("/serie/{serieId}")
    public ResponseEntity<DataDTO> getEpisodeBySerieId(@PathVariable("serieId") Long serieId) {
        return ResponseEntity.ok()
                .body(episodeService.findEpisodeBySerie(serieId));
    }

    @DeleteMapping("/{episodeId}")
    public ResponseEntity<Boolean> deleteEpisode(@PathVariable("episodeId") Long episodeId) {
        return ResponseEntity.ok()
                .body(episodeService.deleteEpisodeById(episodeId));
    }
}
