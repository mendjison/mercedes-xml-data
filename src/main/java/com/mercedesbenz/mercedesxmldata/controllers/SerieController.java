package com.mercedesbenz.mercedesxmldata.controllers;

import com.mercedesbenz.mercedesxmldata.controllers.dto.SerieDTO;
import com.mercedesbenz.mercedesxmldata.services.SerieService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/series")
@AllArgsConstructor
public class SerieController {

    @Qualifier("serieServiceImpl")
    private SerieService serieService;

    @GetMapping("/{serieId}")
    public ResponseEntity<SerieDTO> getSerieById(@PathVariable("serieId") Long serieId) {

        return ResponseEntity
                .ok()
                .body(serieService.findSerieById(serieId));
    }

    @GetMapping
    public ResponseEntity<List<SerieDTO>> getSeries() {

        return ResponseEntity
                .ok()
                .body(serieService.findAllSeries());
    }

    @DeleteMapping("/{serieId}")
    public ResponseEntity<Boolean> deleteSerieById(@PathVariable("serieId") Long serieId) {

        return ResponseEntity
                .ok()
                .body(serieService.deleteSerieById(serieId));
    }
}
