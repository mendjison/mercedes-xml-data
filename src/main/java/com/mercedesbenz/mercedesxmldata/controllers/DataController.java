package com.mercedesbenz.mercedesxmldata.controllers;

import com.mercedesbenz.mercedesxmldata.services.DataService;
import com.mercedesbenz.mercedesxmldata.services.xmlData.DataXml;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/data")
@AllArgsConstructor
public class DataController {

    @Qualifier("dataServiceImpl")
    private DataService dataService;

    @GetMapping
    public ResponseEntity<List<DataXml>> findAllData() throws JAXBException, IOException {
        return ResponseEntity.ok()
                .body(dataService.findAll());
    }

    @GetMapping("/{filename}")
    public ResponseEntity<DataXml> findByFilename(@PathVariable("filename") String filename) throws JAXBException, IOException {
        return ResponseEntity.ok()
                .body(dataService.findDataByFilename(filename));
    }
}
