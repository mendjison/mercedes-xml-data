package com.mercedesbenz.mercedesxmldata.model.enums;

public enum AirsDayOfWeek {
    Monday("Monday"),
    Tuesday("Tuesday"),
    Wednesday("Wednesday"),
    Thursday("Thursday"),
    Friday("Friday"),
    Saturday("Saturday"),
    Sunday("Sunday");

    private String value;

    AirsDayOfWeek(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
