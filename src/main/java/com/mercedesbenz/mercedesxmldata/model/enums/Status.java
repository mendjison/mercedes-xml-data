package com.mercedesbenz.mercedesxmldata.model.enums;

public enum Status {
    Continuing("Continuing"),
    Ended("Ended");
    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
