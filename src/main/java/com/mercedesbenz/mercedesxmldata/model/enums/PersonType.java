package com.mercedesbenz.mercedesxmldata.model.enums;

import javax.xml.bind.annotation.XmlType;

public enum PersonType {
    ACTOR("Actor"),
    GUEST_STAR("GUEST_STAR"),
    WRITER("WRITER  "),
    DIRECTOR("Director"),
    DEFAULT("");
    private String value;

    PersonType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
