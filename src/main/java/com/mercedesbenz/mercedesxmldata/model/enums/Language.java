package com.mercedesbenz.mercedesxmldata.model.enums;

public enum Language {
    en("en"),
    de("de"),
    fr("fr");
    private String value;

    Language(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
