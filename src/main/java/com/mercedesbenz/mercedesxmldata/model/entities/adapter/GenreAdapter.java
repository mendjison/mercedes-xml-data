package com.mercedesbenz.mercedesxmldata.model.entities.adapter;


import com.mercedesbenz.mercedesxmldata.model.entities.Genre;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GenreAdapter extends XmlAdapter<String, List<Genre>> {
    @Override
    public List<Genre> unmarshal(String genreString) throws Exception {
        if (Objects.isNull(genreString)) {
            return new ArrayList<>();
        }
        String[] genres = genreString.split("\\|");
        return Arrays.stream(genres)
                .filter(genre -> Objects.nonNull(genre) && !genre.isEmpty())
                .map(genre -> new Genre(genre))
                .collect(Collectors.toList());
    }

    @Override
    public String marshal(List<Genre> genres) throws Exception {
        if (Objects.isNull(genres) || genres.isEmpty()) {
            return null;
        }
        StringBuilder genreString = new StringBuilder("|");
        for (Genre genre:genres) {
            genreString.append(genre.getGenre())
                    .append("|");
        }
        return genreString.toString();
    }
}
