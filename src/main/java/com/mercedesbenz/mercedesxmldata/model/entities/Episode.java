package com.mercedesbenz.mercedesxmldata.model.entities;

import com.mercedesbenz.mercedesxmldata.controllers.dto.EpisodeDTO;
import com.mercedesbenz.mercedesxmldata.model.entities.adapter.LocalDateAdapter;
import com.mercedesbenz.mercedesxmldata.model.entities.adapter.LocalDateTimeAdapter;
import com.mercedesbenz.mercedesxmldata.model.entities.adapter.PersonAdapter;
import com.mercedesbenz.mercedesxmldata.model.enums.Language;
import jakarta.persistence.*;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@Entity
public class Episode {
    @Id
    private long id;
    @XmlElement(name = "Combined_episodenumber")
    private int combinedEpisodenumber;
    @XmlElement(name = "Combined_season")
    private int combinedSeason;
    @XmlElement(name = "DVD_chapter")
    private String dvdChapter;
    @XmlElement(name = "DVDDiscid")
    private String dvd_discid;
    @XmlElement(name = "DVD_episodenumber")
    private String dvdEpisodenumber;
    @XmlElement(name = "DVD_season")
    private String dvdSeason;
    @XmlElement(name = "Director")
    @XmlJavaTypeAdapter(value = PersonAdapter.class)
    @ManyToMany
    private List<Person> director;
    @XmlElement(name = "EpImgFlag")
    private int epImgFlag;
    @XmlElement(name = "EpisodeName")
    private String episodeName;
    @XmlElement(name = "EpisodeNumber")
    private int episodeNumber;
    @XmlElement(name = "FirstAired")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate firstAired;
    @XmlElement(name = "GuestStars")
    @XmlJavaTypeAdapter(value = PersonAdapter.class)
    @ManyToMany
    private List<Person> guestStar = new ArrayList<>();
    @XmlElement(name = "IMDB_ID")
    private String imdbID;
    @XmlElement(name = "Language")
    @Enumerated(value = EnumType.STRING)
    private Language language;
    @XmlElement(name = "Overview")
    @Lob
    private String overview;
    @XmlElement(name = "ProductionCode")
    private String productionCode;
    @XmlElement(name = "Rating")
    private double rating;
    @XmlElement(name = "RatingCount")
    private int ratingCount;
    @XmlElement(name = "SeasonNumber")
    private int seasonNumber;
    @XmlElement(name = "Writer")
    @XmlJavaTypeAdapter(value = PersonAdapter.class)
    @ManyToMany
    private List<Person> writers = new ArrayList<>();
    @XmlElement(name = "absolute_number")
    private int absoluteNumber;
    @XmlElement(name = "airsafter_season")
    private int airsafterSeason;
    @XmlElement(name = "airsbefore_episode")
    private int airsbeforeEpisode;
    @XmlElement(name = "airsbefore_season")
    private int airsbeforeSeason;
    @XmlElement(name = "filename")
    private String filename;
    @XmlElement(name = "is_movie")
    private int isMovie;
    @XmlElement(name = "lastupdated")
    private long lastupdated;
    @XmlElement(name = "seasonid")
    private long seasonid;
    @XmlElement(name = "seriesid")
    private long seriesid;
    @XmlElement(name = "thumb_added")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime thumbAdded;
    @XmlElement(name = "thumb_height")
    private long thumbHeight;
    @XmlElement(name = "thumb_width")
    private long thumbWidth;
    @ManyToOne
    @XmlTransient
    private Serie serie;

    public EpisodeDTO toDto() {
        return EpisodeDTO.builder()
                .id(this.getId())
                . combinedEpisodenumber(this.getCombinedEpisodenumber())
                .combinedSeason(this.getCombinedSeason())
                .dvdChapter(this.getDvdChapter())
                .dvd_discid(this.getDvd_discid())
                .dvdEpisodenumber(this.getDvdEpisodenumber())
                .dvdSeason(this.getDvdSeason())
                .director(this.getDirector().stream().map(director -> director.getName()).collect(Collectors.toList()))
                .epImgFlag(this.getEpImgFlag())
                .episodeName(this.getEpisodeName())
                .episodeNumber(this.getEpisodeNumber())
                .firstAired(this.getFirstAired())
                .guestStar(this.getGuestStar().stream().map(guestStar -> guestStar.getName()).collect(Collectors.toList()))
                . imdbID(this.getImdbID())
                .language(this.getLanguage())
                .overview(this.getOverview())
                .productionCode(this.getProductionCode())
                .rating(this.getRating())
                .ratingCount(this.getRatingCount())
                .seasonNumber(this.getSeasonNumber())
                .writers(this.getWriters().stream().map(writer -> writer.getName()).collect(Collectors.toList()))
                .absoluteNumber(this.getAbsoluteNumber())
                .airsafterSeason(this.getAirsafterSeason())
                .airsbeforeEpisode(this.getAirsbeforeEpisode())
                .airsbeforeSeason(this.getAirsbeforeSeason())
                .filename(this.getFilename())
                .isMovie(this.getIsMovie())
                .lastupdated(this.getLastupdated())
                .seasonid(this.getSeasonid())
                .seriesid(this.getSeriesid())
                .thumbAdded(this.getThumbAdded())
                .thumbHeight(this.getThumbHeight())
                .thumbWidth(this.getThumbWidth())
                .build();
    }
}
