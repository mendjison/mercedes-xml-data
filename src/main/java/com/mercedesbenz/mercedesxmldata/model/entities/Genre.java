package com.mercedesbenz.mercedesxmldata.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Genre {
    @Id
    private String genre;
}
