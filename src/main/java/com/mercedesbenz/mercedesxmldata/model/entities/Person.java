package com.mercedesbenz.mercedesxmldata.model.entities;

import com.mercedesbenz.mercedesxmldata.model.enums.PersonType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Person {
    @Id
    private String name;
    @Enumerated(EnumType.STRING)
    private PersonType personType;
}
