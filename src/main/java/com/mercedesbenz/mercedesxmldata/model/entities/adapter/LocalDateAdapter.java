package com.mercedesbenz.mercedesxmldata.model.entities.adapter;


import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    @Override
    public LocalDate unmarshal(String datum) throws Exception {

        if (Objects.isNull(datum)) {
            return null;
        }
        try {
            return LocalDate.parse(datum, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (Exception ex) {
            return LocalDate.now();
        }
    }

    @Override
    public String marshal(LocalDate localDate) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return localDate.format(formatter);
    }
}
