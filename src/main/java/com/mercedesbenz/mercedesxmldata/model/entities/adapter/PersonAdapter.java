package com.mercedesbenz.mercedesxmldata.model.entities.adapter;

import com.mercedesbenz.mercedesxmldata.model.entities.Person;
import com.mercedesbenz.mercedesxmldata.model.enums.PersonType;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PersonAdapter extends XmlAdapter<String, List<Person>> {
    @Override
    public List<Person> unmarshal(String personString) throws Exception {

        if (Objects.isNull(personString)) {
            return new ArrayList<>();
        }
        String[] peronNames = personString.replaceAll("\\|", ",").split(",");
        return Arrays.stream(peronNames)
                .filter(personName -> Objects.nonNull(personName) && !personName.isEmpty())
                .map( name -> new Person(name, PersonType.DEFAULT))
                .collect(Collectors.toList());
    }
// \|
    @Override
    public String marshal(List<Person> personList) throws Exception {
        if (Objects.isNull(personList) || personList.isEmpty()) {
            return null;
        }

        if(personList.size() == 1) {
            return personList.get(0).getName();
        }

        StringBuilder personString = new StringBuilder("|");
        for (Person person:personList) {
            personString.append(person)
                    .append("|");
        }
        return personString.toString();
    }
}
