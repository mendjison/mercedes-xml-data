package com.mercedesbenz.mercedesxmldata.model.entities;

import com.mercedesbenz.mercedesxmldata.controllers.dto.SerieDTO;
import com.mercedesbenz.mercedesxmldata.model.entities.adapter.*;
import com.mercedesbenz.mercedesxmldata.model.enums.AirsDayOfWeek;
import com.mercedesbenz.mercedesxmldata.model.enums.Language;
import com.mercedesbenz.mercedesxmldata.model.enums.Status;
import jakarta.persistence.*;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@Entity
public class Serie {
    @Id
    private long id;
    @XmlElement(name = "Actors")
    @XmlJavaTypeAdapter(value = PersonAdapter.class)
    @ManyToMany
    private List<Person> actors = new ArrayList<>();
    @XmlElement(name = "Airs_DayOfWeek")
    @Enumerated(value = EnumType.STRING)
    private AirsDayOfWeek airsDayOfWeek;
    @XmlElement(name = "Airs_Time")
    @XmlJavaTypeAdapter(value = LocalTimeAdapter.class)
    private LocalTime airsTime;
    @XmlElement(name = "ContentRating")
    private String contentRating;
    @XmlElement(name = "FirstAired")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate firstAired;
    @XmlJavaTypeAdapter(value = GenreAdapter.class)
    @XmlElement(name = "Genre")
    @ManyToMany
    private List<Genre> genres = new ArrayList<>();
    @XmlElement(name = "IMDB_ID")
    private String imdbId;
    @XmlElement(name = "Language")
    @Enumerated(EnumType.STRING)
   private Language language;
    @XmlElement(name = "Network")
    private String network;
    @XmlElement(name = "NetworkID")
    private String networkID;
    @XmlElement(name = "Overview")
    @Lob
    private String overview;
    @XmlElement(name = "Rating")
    private double rating;
    @XmlElement(name = "RatingCount")
    private int ratingCount;
    @XmlElement(name = "Runtime")
    private int runtime;
    @XmlElement(name = "SeriesID")
    private long seriesID;
    @XmlElement(name = "SeriesName")
    private String seriesName;
    @XmlElement(name = "Status")
    @Enumerated(EnumType.STRING)
    private Status status;
    @XmlElement(name = "added")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime added;
    @XmlElement(name = "addedBy")
    private long addedBy;
    @XmlElement(name = "banner")
    private String banner;
    @XmlElement(name = "fanart")
    private String fanart;
    @XmlElement(name = "finale_aired")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate  finaleAired;
    private long lastupdated;
    private String poster;
    @XmlElement(name = "tms_wanted_old")
    private int tmsWanteOld;
    @XmlElement(name = "zap2it_id")
    private int zap2itID;

    public SerieDTO toDto() {
        return SerieDTO.builder()
                .id(this.getId())
                .actors(this.getActors().stream().map(actor -> actor.getName()).collect(Collectors.toList()))
                .airsDayOfWeek(this.getAirsDayOfWeek())
                .airsTime(this.getAirsTime())
                .contentRating(this.getContentRating())
                .firstAired(this.getFirstAired())
                .genres(this.getGenres().stream().map(genre -> genre.getGenre()).collect(Collectors.toList()))
                .imdbId(this.getImdbId())
                .language(this.getLanguage())
                .network(this.getNetwork())
                .overview(this.getOverview())
                .rating(this.getRating())
                .ratingCount(this.getRatingCount())
                .runtime(this.getRuntime())
                .seriesID(this.getSeriesID())
                .seriesName(this.getSeriesName())
                .status(this.getStatus())
                .added(this.getAdded())
                .addedBy(this.getAddedBy())
                .banner(this.getBanner())
                .fanart(this.getFanart())
                .finaleAired(this.getFinaleAired())
                .lastupdated(this.getLastupdated())
                .poster(this.getPoster())
                .tmsWanteOld(this.getTmsWanteOld())
                .zap2itID(this.getZap2itID())
                .build();
    }
}
