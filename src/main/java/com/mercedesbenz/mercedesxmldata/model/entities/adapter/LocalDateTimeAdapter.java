package com.mercedesbenz.mercedesxmldata.model.entities.adapter;


import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {
    private final String format= "yyyy-MM-dd HH:mm:ss";
    @Override
    public LocalDateTime unmarshal(String datum) throws Exception {

        if (Objects.isNull(datum)) {
            return null;
        }
        try {
            return LocalDateTime.parse(datum, DateTimeFormatter.ofPattern(format));
        } catch (Exception ex) {
            return LocalDateTime.now();
        }
    }

    @Override
    public String marshal(LocalDateTime localDateTime) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDateTime.format(formatter);
    }
}
