package com.mercedesbenz.mercedesxmldata.model.entities.adapter;


import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {
    private final String format= "HH:mm a";
    @Override
    public LocalTime unmarshal(String datum) throws Exception {

        if (Objects.isNull(datum)) {
            return null;
        }
        try {
            return LocalTime.parse(datum, DateTimeFormatter.ofPattern(format));
        } catch (Exception ex) {
            return LocalTime.now();
        }
    }

    @Override
    public String marshal(LocalTime localDate) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDate.format(formatter);
    }
}
