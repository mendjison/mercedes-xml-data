package com.mercedesbenz.mercedesxmldata.model.repositories;

import com.mercedesbenz.mercedesxmldata.model.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
}
