package com.mercedesbenz.mercedesxmldata.model.repositories;

import com.mercedesbenz.mercedesxmldata.model.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, String> {
}
