package com.mercedesbenz.mercedesxmldata.model.repositories;

import com.mercedesbenz.mercedesxmldata.model.entities.Episode;
import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EpisodeRepository extends JpaRepository<Episode, Long> {

    @Query(value = "SELECT * FROM EPISODE  where serie_id = :serieId", nativeQuery = true)
    List<Episode> findEpisodeBySerie(@Param("serieId") Long serieId);
}
