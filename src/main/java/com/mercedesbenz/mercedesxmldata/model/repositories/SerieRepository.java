package com.mercedesbenz.mercedesxmldata.model.repositories;

import com.mercedesbenz.mercedesxmldata.model.entities.Serie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SerieRepository extends JpaRepository<Serie, Long> {
}
